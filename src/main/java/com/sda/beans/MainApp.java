package com.sda.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        HelloSpring obj = (HelloSpring) context.getBean("helloSpring");
        obj.getMessage();
    }
}
